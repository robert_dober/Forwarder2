# frozen_string_literals: true

module Forwarder
  Version = '0.1.0'
end # module Forwarder
# SPDX-License-Identifier: Apache-2.0

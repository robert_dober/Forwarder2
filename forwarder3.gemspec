# frozen_string_literal: true

$:.unshift( File.expand_path( "../lib", __FILE__ ) )
require 'forwarder/version'
Gem::Specification.new do |s|
  s.name        = 'forwarder3'
  s.version     = Forwarder::Version
  s.summary     = "Delegation... Readable"
  s.description = %{Ruby's core Forwardable gets the job done(barely) and produces most unreadable code.}

  s.authors     = ["Robert Dober"]
  s.email       = 'robert.dober@gmail.com'
  s.files       = Dir.glob("lib/**/*.rb")
  s.files      += %w{LICENSE README.md}
  s.homepage    = 'https://gitlab.com/robert_dober/Forwarder2'
  s.licenses    = %w{Apache 2.0}

  # s.add_dependency 'lab419_core', '~> 0.0.3'

  s.required_ruby_version = '>= 3.1.0'
end
# SPDX-License-Identifier: Apache-2.0

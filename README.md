# Forwarder3

This implementation is for, and needs, Ruby 3.1

## Abstract

Ruby's core Forwardable gets the job done(barely), but produces most unreadable code.

## License

This software is released under the Apache 2.0 License

## Performance

Execution time is that of 85~95% of Forwardable by evalling strings whenever possible.

## How does it work?

Let us [speculate about](https://github.com/RobertDober/speculate_about) that:


### As simple as it gets

Given a class

```ruby
require 'forwarder'
class MyClass
  extend Forwarder

  attr_reader :string

  forward :length, to: :@string
  forward :len, to: :string, as: :length

  def initialize; @string = 'Robert' end
end
```

Then it forwards as expected
```ruby
  expect(MyClass.new.length).to eq(6)
  expect(MyClass.new.len).to eq(6)
```


## LICENSE

Copyright 2022,3 Robert Dober robert.dober@gmail.com,

Apache-2.0 [c.f LICENSE](LICENSE)

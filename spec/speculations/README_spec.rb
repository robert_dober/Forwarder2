# DO NOT EDIT!!!
# This file was generated from "README.md" with the speculate_about gem, if you modify this file
# one of two bad things will happen
# - your documentation specs are not correct
# - your modifications will be overwritten by the speculate rake task
# YOU HAVE BEEN WARNED
RSpec.describe "README.md" do
  # README.md:26
  require 'forwarder'
  class MyClass
  extend Forwarder

  attr_reader :string

  forward :length, to: :@string
  forward :len, to: :string, as: :length

  def initialize; @string = 'Robert' end
  end
  it "it forwards as expected (README.md:41)" do
    expect(MyClass.new.length).to eq(6)
    expect(MyClass.new.len).to eq(6)
  end
end